public class Library {
    private Main[] books;

    public Library(Main[] books) {
        this.books = books;
    }

    public Main[] findByAuthor(String author) {
        Main[] result = new Main[books.length];
        int count = 0;
        for (int i = 0; i < books.length; i++) {
            Main book = books[i];
            if(book != null && contains(book.getAuthors(), author)){
                result[count++] = book;
            }
        }

        Main[] destination = extractSubArray(result, count);

        return destination;
    }

    private boolean contains(String[] items, String target){
        for (String item: items) {
            if(item != null && item.equals(target)){
                return true;
            }
        }
        return false;
    }

    public Main[] findByPublisher(String publisher) {
        Main[] result = new Main[books.length];
        int count = 0;
        for (int i = 0; i < books.length; i++) {
            Main book = books[i];
            if(book != null && book.getPublisher() != null && book.getPublisher().equals(publisher)){
                result[count++] = book;
            }
        }

        Main[] destination = extractSubArray(result, count);

        return destination;
    }

    public Main[] findByYear(int year) {
        Main[] result = new Main[books.length];
        int count = 0;
        for (int i = 0; i < books.length; i++) {
            Main book = books[i];
            if(book != null && book.getYear() == year){
                result[count++] = book;
            }
        }

        Main[] destination = extractSubArray(result, count);

        return destination;
    }

    private Main[] extractSubArray(Main[] source, int count) {
        Main[] destination = new Main[count];
        System.arraycopy(source, 0, destination, 0, count);
        return destination;
    }


}
