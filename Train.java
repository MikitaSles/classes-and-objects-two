import java.util.Arrays;
import java.util.Scanner;

public class Train {
    private String destination;
    private int trainNumber;
    private String departureTime;

    public Train(String destination, int trainNumber, String departureTime) {
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
    }

    public String getDestination() {
        return destination;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public static void main(String[] args) {
        Train[] trains = new Train[5];
        trains[0] = new Train("Minsk", 124, "10:30");
        trains[1] = new Train("Grodno", 232, "12:20");
        trains[2] = new Train("Vilnius", 311, "08:15");
        trains[3] = new Train("Krakow", 464, "14:05");
        trains[4] = new Train("Lvov", 555, "09:40");

        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите номер поезда: ");
        int trainNumber = scanner.nextInt();

        Arrays.sort(trains, (train1, train2) -> train1.getTrainNumber() - train2.getTrainNumber());
        for (Train train : trains) {
            System.out.println(train.getTrainNumber() + " " + train.getDestination() + " " + train.getDepartureTime());
        }

        int index = binarySearch(trains, trainNumber);
        if (index == -1) {
            System.out.println("Поезд с номером " + trainNumber + " не найден");
        } else {
            Train train = trains[index];
            System.out.println("Найден поезд: " + train.getTrainNumber() + " " + train.getDestination() + " " + train.getDepartureTime());
        }

        Arrays.sort(trains, (train1, train2) -> {
            int destinationCompare = train1.getDestination().compareTo(train2.getDestination());
            if (destinationCompare != 0) {
                return destinationCompare;
            } else {
                return train1.getDepartureTime().compareTo(train2.getDepartureTime());
            }
        });
        for (Train train : trains) {
            System.out.println(train.getTrainNumber() + " " + train.getDestination() + " " + train.getDepartureTime());
        }
    }

    private static int binarySearch(Train[] trains, int trainNumber) {
        int left = 0;
        int right = trains.length - 1;
        while (left <= right) {
            int middle = (left + right) / 2;
            if (trains[middle].getTrainNumber() == trainNumber) {
                return middle;
            } else if (trains[middle].getTrainNumber() < trainNumber) {
                left = middle + 1;
            } else {
                right = middle - 1;
            }
        }
        return -1;
    }
}
