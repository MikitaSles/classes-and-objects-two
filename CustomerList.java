import java.util.Arrays;

public class CustomerList {
    private Customer[] customers;

    public CustomerList(Customer[] customers) {
        this.customers = customers;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    public void sortBySurname() {
        Arrays.sort(customers, (c1, c2) -> c1.getSurname().compareTo(c2.getSurname()));
    }

    public Customer[] getCustomersByCreditCardNumber(String start, String end) {
        return Arrays.stream(customers)
                .filter(c -> c.getCreditCardNumber().compareTo(start) >= 0 && c.getCreditCardNumber().compareTo(end) <= 0)
                .toArray(Customer[]::new);
    }
}
