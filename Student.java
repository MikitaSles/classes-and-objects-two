public class Student {
    private String surnameAndInitials;
    private int groupNumber;
    private int[] academicPerformance;

    public Student(String surnameAndInitials, int groupNumber, int[] academicPerformance) {
        this.surnameAndInitials = surnameAndInitials;
        this.groupNumber = groupNumber;
        this.academicPerformance = academicPerformance;
    }

    public String getSurnameAndInitials() {
        return surnameAndInitials;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public int[] getAcademicPerformance() {
        return academicPerformance;
    }
}

